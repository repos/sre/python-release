#!/bin/bash

set -e

export GNUPGHOME=/root/.gnupg
export REPREPRO_BASE_DIR=/srv/wikimedia
BUILD_HOST="build2001.codfw.wmnet"

if [[ "${#}" -lt "5" ]]; then
    echo "Usage: ${0} PACKAGE_NAME SOURCE_PACKAGE_NAME DISTRO APT_COMPONENT VERSION [VARIANT]"
    exit 1
fi

PACKAGE_NAME="${1}"
SOURCE_PACKAGE_NAME="${2}"
DISTRO="${3}"
APT_COMPONENT="${4}"
VERSION="${5}"
VARIANT="${6:-1}"

mkdir -p "${PACKAGE_NAME}/${DISTRO}/"
sudo rsync -va -i ${BUILD_HOST}::pbuilder-result/${DISTRO}-amd64/${PACKAGE_NAME}* ${BUILD_HOST}::pbuilder-result/${DISTRO}-amd64/${SOURCE_PACKAGE_NAME}* ${PACKAGE_NAME}/${DISTRO}/

sudo -E reprepro --ignore=undefinedtarget -C ${APT_COMPONENT} include ${DISTRO}-wikimedia ${PACKAGE_NAME}/${DISTRO}/${SOURCE_PACKAGE_NAME}*_${VERSION}-${VARIANT}*_amd64.changes

echo "APT UPDATED SUCCESFUL"
