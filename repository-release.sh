#!/bin/bash

set -e

if (( $# < 5 )); then
    echo "Usage: ${0} REPOSITORY_NAME DISTRO BINARY_PACKAGE_NAME SOURCE_PACKAGE_NAME VERSION [VARIANT] [UPDATE]"
    echo "Example: ${0} pywmflib bullseye python3-wmflib python-wmflib 1.2.3 2 3"
    echo "Builds: 1.2.3-2+debNNu3"
    exit 1
fi

function print_line() {
    echo "===> ${1}"
}

REPOSITORY_NAME="${1}"
DISTRO="${2}"
BINARY_PACKAGE_NAME="${3}"
SOURCE_PACKAGE_NAME="${4}"
VERSION="${5}"
VARIANT="${6:-1}"
UPDATE="${7:-1}"
declare -A DISTROS=([bookworm]="deb12u${UPDATE}" [bullseye]="deb11u${UPDATE}" [buster]="deb10u${UPDATE}")

print_line "Cleaning up previous build, if present"
rm -fv "${REPOSITORY_NAME}_${VERSION}"*
echo

# TODO: this only support repos under the operations/software namespace,
# we should expand the logic.
if ! test -d ${REPOSITORY_NAME}; then
  print_line "Repository not checkout in your home dir on ${BUILD_HOST}, cloning it."
  git clone "https://gerrit.wikimedia.org/r/operations/software/${REPOSITORY_NAME}"
fi

pushd ${REPOSITORY_NAME}
git checkout debian
git pull
sed -i "1 s/) unstable/+${DISTROS[$DISTRO]}) ${DISTRO}-wikimedia/" debian/changelog
git add debian/changelog
git commit -m "Set distro to ${DISTRO} (${DISTROS[$DISTRO]}), local modification"
git log -1 -p
echo
GIT_PBUILDER_AUTOCONF=no BUILD_HOME=$BUILDDIR WIKIMEDIA=yes gbp buildpackage -j4 -sa -us -uc --git-pbuilder --git-no-pbuilder-autoconf --git-color=on --git-arch=amd64 --git-dist=${DISTRO}
echo
git reset --hard HEAD^
popd

set +e
print_line "Content of the package for ${DISTRO}:"
dpkg -c "/var/cache/pbuilder/result/${DISTRO}-amd64/${BINARY_PACKAGE_NAME}_${VERSION}-${VARIANT}+${DISTROS[$DISTRO]}_"*.deb
print_line "End of package content for ${DISTRO}"
echo

print_line "Lintian result:"
lintian -IE --profile wikimedia --color=always --show-overrides "/var/cache/pbuilder/result/${DISTRO}-amd64/${SOURCE_PACKAGE_NAME}_${VERSION}-${VARIANT}+${DISTROS[$DISTRO]}_amd64.changes"
echo

print_line "BUILD SUCCESSFUL"
