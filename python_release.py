#!/usr/bin/env python3
"""Perform the release of a Python package in the Wikimedia Foundation infrastructure.

See the README.md file for the complete documentation.
"""
import argparse
import code
import configparser
import hashlib
import logging
import os
import shutil
import subprocess
import sys
import tarfile
import time
from dataclasses import dataclass, field
from datetime import datetime, timezone
from email.utils import formatdate
from getpass import getpass
from pathlib import Path
from typing import Any, Callable, ClassVar, Dict, List, Tuple
from urllib.request import urlretrieve

import github3

CONFIG_FILENAME = '.wmfconfig'
BUILD_HOST = 'build2001.codfw.wmnet'  # TODO: get it dynamically
APT_HOST = 'apt1002.wikimedia.org'  # TODO: get it dynamically
GITHUB_RELEASE_TEMPLATE = """[Release Notes](CHANGELOG.rst)

* `{source}` is the signature of the Source Code tar.gz archive found in the Release page of the GitHub website.
* `{tarball}` is the signature of the tarball downloaded from the GitHub Release APIs, that has a different top-level directory name.
"""  # noqa: E501
logger = logging.getLogger()


class ReleaseError(Exception):
    """Custom exception raised if the release should not be made."""


class AbortError(Exception):
    """Custom exception raised to abort the execution."""


@dataclass(order=True)
class Version:
    """Represent a semantic version of the form MAJOR.MINOR.PATCH. Does not support pre-releases."""

    major: int
    minor: int
    patch: int

    def __str__(self) -> str:
        """String representation of the version."""
        return f'{self.major}.{self.minor}.{self.patch}'

    @property
    def release(self) -> str:
        """Release name for this version."""
        return f'v{self}'

    @property
    def zero(self) -> bool:
        """Return true if this is the zero version."""
        return self.major == self.minor == self.patch == 0

    def check_transition(self, new: 'Version') -> None:
        """Validate the transition from the current version to the new one, raise ReleaseError on error."""
        try:
            if new.major == self.major:
                if new.minor == self.minor:
                    assert new.patch == self.patch + 1, 'PATCH'
                else:
                    assert new.minor == self.minor + 1, 'MINOR'
                    assert new.patch == 0, 'patch'
            else:
                assert new.major == self.major + 1, 'MAJOR'
                assert new.minor == 0, 'MINOR'
                assert new.patch == 0, 'PATCH'
        except AssertionError as e:
            raise ReleaseError(f'Illegal transition between {self} and {new} versions: wrong {e}') from e
        else:
            logger.info('Approved version transition between %s and %s', self, new)


def get_version(version: str) -> Version:
    """Return a Version instance from a string version."""
    return Version(*[int(i) for i in version.split('.')])


@dataclass
class Command:
    """Represent a callable command to be executed."""

    choices: ClassVar[List[str]] = ['retry', 'skip', 'abort', 'debug']
    """Available options for the user on failure."""
    input_message: ClassVar[str] = (
        'What do you want to do? "retry" the last command, "skip" the last command but '
        'continue, "abort" the execution, "debug" drop into a REPL shell'
    )
    """Message for the user on failure."""

    func: Callable
    args: Tuple = field(default_factory=tuple)
    kwargs: Dict = field(default_factory=dict)
    report_only: bool = field(default_factory=bool)

    def __str__(self) -> str:
        """Return the string representation of the command and its parameters."""
        args = [str(arg) for arg in self.args]
        args.extend(f'{key}={value}' for key, value in self.kwargs.items())
        args_repr = ', '.join(args)
        return f'{self.func.__module__}.{self.func.__name__}({args_repr})'

    def run(self) -> None:
        """Execute the command."""
        while True:
            try:
                self.func(*self.args, **self.kwargs)
            except AbortError:
                logger.info('Execution aborted')
                raise
            except BaseException as e:  # pylint: disable=broad-except
                logger.error('Failed to run %s: %s', self, e)
                logger.debug('Traceback', exc_info=True)
                response = ask_input(Command.input_message, Command.choices)
                if response == 'skip':
                    return
                if response == 'abort':
                    raise
                if response == 'debug':
                    code.interact(local=locals())
            else:
                return


class ExecutionPlan:
    """Represent an execution plan of multiple Command instances."""

    def __init__(self) -> None:
        """Initialize the instance."""
        self._commands: List[Command] = []
        self._tag_step = 1

    def __len__(self) -> int:
        """Return the number of commands registered in the execution plan."""
        return len(self._commands)

    @property
    def tag_step(self) -> int:
        """Return the step number that generates the release tag."""
        return self._tag_step

    def mark_tag(self) -> None:
        """Mark the last registered command as the tag step."""
        self._tag_step = len(self)

    def add(self, func: Callable, *args: Any, **kwargs: Any) -> None:
        """Add a callable command to the execution plan with its arguments."""
        self._commands.append(Command(func, args=args, kwargs=kwargs, report_only=False))

    def add_report_only(self, func: Callable, *args: Any, **kwargs: Any) -> None:
        """Add a callable command to the execution plan with its arguments and mark it as report only."""
        self._commands.append(Command(func, args=args, kwargs=kwargs, report_only=True))

    def cmd(self, *args: Any) -> None:
        """Add a subprocess.run command to the execution plan with its arguments."""
        self.add(subprocess.run, list(args), check=True)

    def git(self, *args: Any) -> None:
        """Add a git command to the execution plan with its arguments."""
        self.cmd('/usr/bin/git', *args)

    def confirm(self, message: str) -> None:
        """Add to the execution plan a user confirmation stopgap before proceeding."""
        self.add_report_only(ask_confirmation, message)

    def log(self, message: str, level: int = logging.INFO) -> None:
        """Add to the execution plan a log message."""
        self.add_report_only(logger.log, level, message)

    def run(self, start_at: int = 1, dry_run: bool = False) -> None:
        """Run the execution plan, enumerating the steps and starting at the given start_at step."""
        if start_at > len(self._commands):
            raise ReleaseError(f'Invalid start step {start_at}, there are only {len(self._commands)} steps')

        for step, command in enumerate(self._commands, 1):
            if step < start_at:
                logger.debug('Skipping step %d, start at set to %d', step, start_at)
                continue

            if not command.report_only or dry_run:
                logger.info('\x1b[32mStep %d\x1b[39m: %s', step, command)

            if not dry_run:
                command.run()


def ask_input(message: str, choices: List) -> str:
    """Ask the user for input in interactive mode.

    Arguments:
        message (str): the message to be printed before asking for confirmation.
        choices (list): the available choices of possible answers that the user can give.

    Returns:
        str: the selected choice.

    Raises:
        RuntimeError: on too many invalid answers or if not in a TTY.

    """
    prefix = '\x1b[32m>>>\x1b[39m'
    if not sys.stdout.isatty():
        raise RuntimeError('Not in a TTY, unable to ask for confirmation')

    print(f'{prefix} {message}')

    for _ in range(3):
        resp = input('> ')
        if resp in choices:
            return resp

        print(
            f'{prefix} Invalid response, please type one of {choices}. '
            f'After 3 wrong answers the task will be aborted.'
        )

    raise RuntimeError('Too many invalid answers')


def ask_confirmation(message: str) -> None:
    """Ask the user for confirmation in interactive mode.

    Arguments:
        message (str): the message to be printed before asking for confirmation.

    Raises:
        RuntimeError: on too many invalid answers or if not in a TTY.

    """
    response = ask_input(
        '\n'.join((message, 'Type "go" to proceed or "abort" to interrupt the execution')), ['go', 'abort']
    )
    if response == 'abort':
        raise AbortError


def get_current_version() -> Version:
    """Return the current version from the last git tag."""
    result = subprocess.run(
        ['/usr/bin/git', 'describe', '--tags', '--abbrev=0'], capture_output=True, text=True, check=True)
    if result.returncode == 0:
        return get_version(result.stdout.strip()[1:])

    if 'cannot describe anything' in result.stderr:
        return get_version('0.0.0')  # There is no previous release

    raise ReleaseError(f'Unable to determine last release: {result.stderr}')


def update_changelog(release: str, repository: str, github_repository: str) -> None:
    """Update the CHANGELOG.rst file with the entry for the new release."""
    changelog = Path('CHANGELOG.rst')
    changelog.touch()  # Create an empty file if missing
    existing = changelog.read_text()
    existing_lines = existing.splitlines()

    def gen_heading(heading: str, separator: str) -> List[str]:
        return [heading, separator * len(heading), '']

    lines = []
    if not existing_lines:
        lines.extend(gen_heading(f'{repository} Changelog', '-'))
    else:
        lines.extend(existing_lines[:3])

    today = datetime.now(tz=timezone.utc).date()
    lines.extend(gen_heading(f'`{release}`_ ({today})', '^'))

    paragraphs = (
        '{API|CLI|Configuration|Dependencies} breaking changes',
        'New features',
        'Minor improvements',
        'Bug fixes',
        'Miscellanea',
    )
    for paragraph in paragraphs:
        lines.extend(gen_heading(paragraph, '"'))

    lines.extend(existing_lines[3:])  # Append the remaining existing lines
    lines.append(f'.. _`{release}`: https://github.com/{github_repository}/releases/tag/{release}')
    changelog.write_text('\n'.join(lines))


def download_from_github(release: str, github_repository: str, path: Path) -> None:
    """Download artifacts from GitHub."""
    url = f'https://github.com/{github_repository}/archive/{release}.tar.gz'
    logger.info('Downloading the GitHub tag archive %s into %s', url, path)
    for _ in range(10):
        try:
            urlretrieve(url, path)
            logger.info('Saved GitHub archive to %s', path)
        except Exception:  # pylint: disable=broad-except
            logger.info('Waiting for the GitHub archive to become available...')
            time.sleep(10)
        else:
            return

    raise RuntimeError(f'Unable to find the GitHub archive at {url}')


def sha256(obj: bytes) -> str:
    """Return the sha256 has of a binary content."""
    buf = hashlib.sha256()
    buf.update(obj)
    return buf.hexdigest()


def get_archive_hashes(file: Path) -> List[str]:
    """Return a list of hahses for each member of a compressed archive."""
    tar = tarfile.open(file)
    return [sha256(member.tobuf()) for member in tar.getmembers()]


def compare_archives(a_file: Path, b_file: Path) -> None:
    """Verify that two archives are indentical performing the checksum of all their files."""
    a_digests = get_archive_hashes(a_file)
    b_digests = get_archive_hashes(b_file)
    a_len = len(a_digests)
    b_len = len(b_digests)

    if a_len != b_len:
        raise RuntimeError(f'Archive {a_file} number of files does not match archive {b_file}: {a_len} != {b_len}')

    if sorted(a_digests) != sorted(b_digests):
        diff = set(a_digests).symmetric_difference(set(b_digests))
        raise RuntimeError(f'Archive {a_file} member checksums does not match the ones in {b_file}: {diff}')


def create_github_release(
    repo: github3.repos.repo.Repository, tag: str, source_signature: Path, tarball_path: Path
) -> None:
    """Create a new release on GitHub from the given tag, attaching the archive signatures to it."""
    logger.info('Creating release %s', tag)
    try:
        release = repo.create_release(
            tag, name=tag, body=GITHUB_RELEASE_TEMPLATE.format(source=source_signature.name, tarball='TBD')
        )
    except Exception as e:
        logger.error(e.errors if hasattr(e, 'errors') else e)
        raise

    if not release:
        raise RuntimeError(f'Failed to create release {tag}')

    if not release.archive('tarball', path=str(tarball_path)):
        raise RuntimeError('Failed to download the release tarball')


def update_github_release(repo: github3.repos.repo.Repository, source_signature: Path, tarball_signature: Path) -> None:
    """Update the latest release on GitHub attaching the archive signature to it."""
    release = repo.latest_release()
    if not release.edit(
        body=GITHUB_RELEASE_TEMPLATE.format(source=source_signature.name, tarball=tarball_signature.name)
    ):
        raise RuntimeError(f'Failed to update release {release}')

    for signature in (source_signature, tarball_signature):
        asset = release.upload_asset('application/pgp-signature', signature.name, signature.read_text())
        if asset.id is None:
            raise RuntimeError(f'Failed to upload asset {signature}')

        print(f'Asset {asset.id} created at {asset.created_at} from file {signature.name}: {asset.download_url}')


def update_debian_changelog(src_pkg_name: str, version: Version) -> None:
    """Add an entry to the debian/changelog file."""
    author_details = []
    for config in ('user.name', 'user.email'):
        author_details.append(
            subprocess.run(
                ['/usr/bin/git', 'config', config], check=True, capture_output=True, text=True).stdout.strip()
        )

    debian_changelog_entry = f"""{src_pkg_name} ({version}-1) unstable; urgency=medium

  * {version.release} upstream release.

 -- {author_details[0]} <{author_details[1]}>  {formatdate()}
"""
    changelog = Path('debian/changelog')
    existing = changelog.read_text()
    changelog.write_text(f'{debian_changelog_entry}\n{existing}')


def get_venv_bin() -> Path:
    """Find a valid virtualenv and return its bin directory."""
    tox = Path('.tox')
    if tox.exists():
        for tox_env in tox.iterdir():
            bin_dir = tox_env / 'bin'
            if bin_dir.exists() and (bin_dir / 'python').exists():
                logger.debug('Found virtualenv bin at %s', bin_dir)
                return bin_dir

    raise ReleaseError(
        'Unable to find any valid virtualenv in .tox/, please run tox or equivalent to create it before retrying.'
    )


def run_twine_command(twine: str, cmd: str) -> None:
    """Run the given twine command passing to it the list of all files in the dist/ directory."""
    args = [twine, cmd]
    args.extend(str(item) for item in Path('dist/').iterdir() if item.is_file())
    subprocess.run(args, check=True)


def run_tox_sphinx() -> None:
    """Find and run the first tox environment that generates the Sphinx documentation, if any."""
    result = subprocess.run(['tox', '-a'], capture_output=True, text=True, check=True)
    for line in result.stdout.splitlines():
        if 'sphinx' in line:
            python_executable = f'python{line[2:3]}.{line[3:4]}'
            if shutil.which(python_executable) is None:
                logger.debug('Skipping sphinx environment %s, %s not found in path', line, python_executable)
                continue
            subprocess.run(['tox', '-e', line], check=True)
            break
    else:
        raise ReleaseError('Unable to find a tox environment to test the documentation generation. Continue without?')


def register_release(c: ExecutionPlan, args: argparse.Namespace, config: configparser.ConfigParser) -> None:
    """Register all the commands to make a new release for the repository."""
    release = args.version.release
    if '/' in config['github']['repository']:
        github_repository = config['github']['repository']
        repository = config['github']['repository'].split('/')[1]
        git_review = False
    else:
        github_repository = f"wikimedia/{config['github']['repository']}"
        repository = config['github']['repository']
        git_review = True

    if args.dry_run:
        github_repo = None
    else:
        github_token = getpass('GitHub Token: ')
        github_instance = github3.login(token=github_token)
        github_repo = github_instance.repository(*github_repository.split('/'))

    archive_base_path = Path(args.releases_path)
    archive_filename = f'{args.repository}-{args.version}.tar.gz'
    archive_path = archive_base_path / 'local' / archive_filename
    github_path = archive_base_path / 'github' / archive_filename
    github_tarball_path = archive_base_path / 'github' / f'{args.repository}-{args.release}.tar.gz'

    c.git('checkout', config.get('github', 'main_branch', fallback='master'))
    c.git('pull')
    if not config.getboolean('release', 'skip_changelog', fallback=False):
        c.git('checkout', '-b', f'changelog-{release}')
        c.add(update_changelog, args.version.release, args.repository, github_repository)
        if args.version.zero:
            c.git('--no-pager', 'log')
        else:
            c.git('--no-pager', 'log', f'{args.current_version.release}..HEAD')

        c.confirm(
            'Add to the CHANGELOG.rst file the release notes, remove any unused section, do not commit any '
            'change and there is no need to git add them either.\nFor reference, those above are the commits '
            'since the last tag.'
        )
        c.add(run_tox_sphinx)
        c.git('diff')
        c.confirm('Are the above changes ok to be committed?')
        c.git('add', 'CHANGELOG.rst')
        c.git('commit', '-m', f'CHANGELOG: add changelogs for release {release}')
        if git_review:
            c.git('review')
            c.confirm('Go to Gerrit and merge the change, continue only once fully merged')  # TODO: poll for status
            c.git('checkout', config.get('github', 'main_branch', fallback='master'))
            c.git('pull')
        else:
            c.confirm('Fully merge the change?')
            c.git('checkout', config.get('github', 'main_branch', fallback='master'))
            c.git('rebase', f'changelog-{release}')
            c.git('push')

        c.git('branch', '-d', f'changelog-{release}')

    c.git('tag', '-s', '-a', release, '-m', release)
    c.mark_tag()
    c.git('--no-pager', 'tag')
    c.git('show', release)
    c.confirm('Is the git tag correct?')
    c.git('push', 'origin', release)
    c.git('archive', f'--prefix={repository}-{args.version}/', '-o', str(archive_path), release)
    c.log('Signing the local tar.gz archive')
    c.cmd('gpg', '--armor', '--detach-sign', str(archive_path))
    c.add(download_from_github, release, github_repository, github_path)
    c.add(compare_archives, archive_path, github_path)
    c.log('Signing the GitHub tar.gz archive')
    c.cmd('gpg', '--armor', '--detach-sign', str(github_path))
    c.add(create_github_release, github_repo, release, archive_path.with_suffix('.gz.asc'), github_tarball_path)
    c.log('Signing the GitHub tar.gz release archive')
    c.cmd('gpg', '--armor', '--detach-sign', str(github_tarball_path))
    c.add(
        update_github_release,
        github_repo,
        archive_path.with_suffix('.gz.asc'),
        github_tarball_path.with_suffix('.gz.asc'),
    )


def register_debian(c: ExecutionPlan, args: argparse.Namespace, config: configparser.ConfigParser) -> None:
    """Register all the commands required to perform a Debian release."""
    release = args.version.release
    src_pkg = config['debian']['source_package']
    bin_pkg = config['debian']['binary_package']
    distros = [distro.strip() for distro in config['debian']['distros'].split(",")]
    if 'apt_component' in config['debian']:
        apt_component = config['debian']['apt_component']
    else:
        apt_component = "main"

    c.git('checkout', 'debian')
    c.git('pull')
    c.git('fetch', '--tags', 'origin')
    c.git('merge', '--no-edit', '--no-ff', f'tags/{release}')
    c.git('--no-pager', 'log', '-1')
    c.git('--no-pager', 'log', '--graph', '--decorate', '--pretty=oneline', '--abbrev-commit', f'tags/{release}..HEAD', '-5')
    c.confirm('Verify that the debian branch and the merge is correct')
    c.git('push', 'origin', 'debian')
    c.git('checkout', '-b', f'release-{release}', '--track', 'origin/debian')
    c.git('branch', '-vvv', '--list', f'release-${release}')
    c.add(update_debian_changelog, src_pkg, args.version)
    c.git('diff')
    c.confirm('Modify anything that is needed in the debian/ directory, no need to git add')
    c.git('diff')
    c.confirm('Are the above changes ok to be committed?')
    c.git('add', 'debian/')
    c.git('commit', '-m', f'Upstream release {release}')
    if '/' in config['github']['repository']:  # No git review
        c.confirm('Fully merge the change?')
        c.git('checkout', 'debian')
        c.git('rebase', f'release-{release}')
        c.git('push')
    else:
        c.git('review')
        c.confirm('Go to Gerrit and merge the change, continue only once fully merged')  # TODO: poll for status
        c.git('checkout', 'debian')
        c.git('pull')

    c.git('branch', '-d', f'release-{release}')
    c.confirm(
        f"About to go on {BUILD_HOST}:{args.repository} to build the deb package. "
        "The repo will be automatically cloned if not present, otherwise please "
        "make sure that it is in a good state."
    )
    c.cmd('scp', f"{Path(__file__).parent}/repository-release.sh", f"{BUILD_HOST}:repository-release")
    for distro in distros:
        c.cmd('ssh', BUILD_HOST, f'./repository-release "{args.repository}" "{distro}" "{bin_pkg}" "{src_pkg}" "{args.version}"')

    if src_pkg == bin_pkg:
        packages: Tuple = (src_pkg,)
    else:
        packages = (src_pkg, bin_pkg)

    for pkg_name in packages:
        for distro in distros:
            c.cmd(
                'scp',
                f'{BUILD_HOST}:/var/cache/pbuilder/result/{distro}-amd64/{pkg_name}_{args.version}*',
                f'{args.releases_path}/deb/{distro}/',
            )

    # c.confirm('Signing the Debian package (re-insert the yubikey if applicable)')
    # for distro in distros:
    #     c.cmd('gpg', '--armor', '--detach-sign',
    #           f'{args.releases_path}/deb/{distro}/{bin_pkg}_{args.version}-1"*_amd64.deb')
    # TODO: Upload the Debian package to GitHub ./github-make-release --upload-deb "${VERSION}"

    c.confirm('Releasing the new package to Wikimedia APT?')
    c.cmd('scp', f"{Path(__file__).parent}/apt-release.sh", f"{APT_HOST}:apt-release")
    for distro in distros:
        c.cmd('ssh', APT_HOST, f'./apt-release {bin_pkg} {src_pkg} {distro} {apt_component} "{args.version}"')
    components = ','.join(f'{distro}-wikimedia' for distro in distros)
    c.confirm(
        f'Paste this message to IRC in the #wikimedia-operations channel:\n'
        f'!log uploaded {bin_pkg}_{args.version} to apt.wikimedia.org {components}'
    )
    c.git('checkout', config.get('github', 'main_branch', fallback='master'))


def register_pypi(c: ExecutionPlan, args: argparse.Namespace, config: configparser.ConfigParser) -> None:
    """Register all the commands required to perform a PyPI release."""
    wheels_prefix = config['pypi']['wheels_prefix']

    c.confirm('Releasing the new package version to PyPI?')
    c.add(shutil.rmtree, 'dist/', ignore_errors=True)
    c.add(shutil.rmtree, 'build/', ignore_errors=True)
    c.add(shutil.rmtree, f'{wheels_prefix}.egg-info/', ignore_errors=True)

    venv_bin = get_venv_bin()
    python = str(venv_bin / 'python')
    pip = str(venv_bin / 'pip')
    twine = str(venv_bin / 'twine')

    c.cmd(pip, 'install', 'twine', 'readme_renderer')
    c.cmd(python, 'setup.py', 'sdist')
    c.cmd(python, 'setup.py', 'bdist_wheel')
    c.log('Checking README syntax for PyPI')
    c.add(run_twine_command, twine, 'check')
    c.confirm('Ready to upload the new package to PyPI?')
    c.add(run_twine_command, twine, 'upload')
    c.log('Copying artifacts')
    c.add(shutil.copytree, 'dist/', args.releases_path / 'pypi', dirs_exist_ok=True)
    c.cmd(pip, 'uninstall', '-y', 'twine', 'readme_renderer')


def register(args: argparse.Namespace, config: configparser.ConfigParser) -> ExecutionPlan:
    """Register all the commands to execute and return the execution plan."""
    commands = ExecutionPlan()
    commands.log('### START NEW RELEASE STEPS ###')
    register_release(commands, args, config)
    commands.log('### END NEW RELEASE STEPS ###')

    if 'debian' in config:
        (args.releases_path / 'deb').mkdir(exist_ok=True)
        for distro in config['debian']['distros'].split(','):
            (args.releases_path / 'deb' / distro).mkdir(exist_ok=True)

        commands.log('### START DEBIAN RELEASE STEPS ###')
        register_debian(commands, args, config)
        commands.log('### END DEBIAN RELEASE STEPS ###')

    commands.git('log', '--graph', '--decorate', '--pretty=oneline', '--abbrev-commit', '-1')

    if 'pypi' in config:
        commands.log('### START PYPI RELEASE STEPS ###')
        register_pypi(commands, args, config)
        commands.log('### END PYPI RELEASE STEPS ###')

    return commands


def run() -> int:
    """Make a release and return the exit code."""
    parser = argparse.ArgumentParser(description=__doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('-d', '--debug', action='store_true', help='Debug log level.')
    parser.add_argument(
        '-n', '--dry-run', action='store_true', help='Just print the execution plan. Do not do anything.'
    )
    parser.add_argument(
        '--start-at-step', type=int, default=1, help='Restart from this step instead that from the start.'
    )
    parser.add_argument(
        'repository_path',
        metavar='REPOSITORY',
        type=Path,
        nargs='?',
        default=Path.cwd(),
        help=(
            'The path to the repository root where the project is checked out. If not set the '
            'current working directory is used.'
        ),
    )
    parser.add_argument('version', metavar='VERSION', type=get_version, help='The version to release.')

    args = parser.parse_args()
    log_level = logging.INFO
    if args.debug:
        log_level = logging.DEBUG

    logging.basicConfig(level=log_level, format='%(asctime)s [%(levelname)s]: %(message)s')

    args.release = f'v{args.version}'
    releases_base_path = os.getenv('WMF_PYTHON_RELEASES')
    if releases_base_path is None:
        logger.critical('Unable to get WMF_PYTHON_RELEASES environment variable')
        return 2

    if args.repository_path != Path.cwd():
        os.chdir(args.repository_path)

    config = configparser.ConfigParser()
    if not config.read(CONFIG_FILENAME):
        logger.critical('Unable to load configuration %s from repository %s', CONFIG_FILENAME, args.repository_path)
        return 1

    args.repository = args.repository_path.name
    args.releases_path = Path(releases_base_path) / args.repository
    args.releases_path.mkdir(exist_ok=True)
    args.current_version = get_current_version()
    commands = register(args, config)

    if args.dry_run:
        logger.info('Skipping check of version transition in dry_run mode')
    elif args.start_at_step <= commands.tag_step:
        args.current_version.check_transition(args.version)
    else:
        logger.info('Skipping check of version transition when resuming after the tag has been generated')

    for dirname in ('local', 'github', 'deb', 'pypi'):
        (args.releases_path / dirname).mkdir(exist_ok=True)

    commands.run(start_at=args.start_at_step, dry_run=args.dry_run)
    logger.info('ALL DONE, %s released', args.version.release)
    return 0


if __name__ == '__main__':
    try:
        RET = run()
    except AbortError:
        RET = 97
    except ReleaseError as e:
        logger.error(e)
        RET = 98
    except Exception as e:  # pylint: disable=broad-except
        logger.error('Unexpected error (run with -d/--debug to see the traceback): %s', e)
        logger.debug('Traceback', exc_info=True)
        RET = 99

    sys.exit(RET)
