# python-release

Perform the release of a Python package in the Wikimedia Foundation infrastructure.

It will perform a full release interacting with the user. On failure will ask the user if the step should be retried,
allowing the user to fix anything missing before, skipped because manually performed to continue the release process,
or abort the whole execution. It is also possible to resume an aborted release from a specific step.

## Performed steps

  * Stub the update for the ``CHANGELOG.rst`` file, ask the user to complete it, initiate a code review for it and wait
    until it's merged.
  * Make a new signed git tag and push it to the remote repository.
  * Save all the generated artifacts locally and checksum them when needed.

  * If configured to perform a Debian release:
    * Update the debian branch and setup the ``debian/changelog`` entry, ask the user to complete it, initiate a code
      review for it and wait until the user confirms it's manually merged.
    * Run a Debian build script on the build host and download the generated artifacts.
    * Run a Debian deploy script on the APT host and ask the user to ``!log`` a message on IRC.

  * If configured to perform a PyPI release:
    * Cleanup previous artifacts in the repository
    * Build the package.
    * Check the ``README`` syntax.
    * Release the artifacts to PyPI.

## Repository requirements

  * A file named ``.wmfconfig`` in the root of the repository with the following format:

```
    [release]  # [optional] Defines specific options for the software tag release
    skip_changelog = true  # If the update of a CHANGELOG.rst file should be skipped

    [github]  # Required section
    # The repository name in the Wikimedia's GitHub organization or the full path organization/repository.
    repository = operations-software-a-name  # OR organization/repository

    [pypi]  # [optional] If not present no PyPI release will be made.
    name =  # The name of the PyPi package
    wheels_prefix = a_name  #  The prefix name of the wheels, usually uses underscores instead of dashes.

    [debian]  # [optional] If not present no Debian package will be built and released.
    distros = buster,bullseye  # Comma separated list of Debian distribution names to build and deploy.
                               # The debian/changelog entry uses unstable as the distro name.
    source_package = python-a-name  # The Debian source package name.
    binary_package = python3-a-name  # The Debian binary package name.
    apt_component = thirdparty/something # [optional] The APT component to use when uploading.
```

  * A branch named ``debian`` that is used to keep the ``debian/`` directory and where the ``master`` branch gets
    merged into.
  * A ``CHANGELOG.rst`` file with the changelog entries in the root of the repository.
  * Permissions to push annotated tags to the repository's origin.
  * A versioning system that uses semantic version of the form ``MAJOR.MINOR.PATCH`` (all integers) and ``v{version}``
    for the release names (e.g. ``v0.0.1``)

## Local requirements

  * Python 3.9+
  * The ``github3`` Python package installed.
  * An environment variable ``WMF_PYTHON_RELEASES`` with the path to a local directory where to save all the generated
    artifacts. The sub-tree for any given repository will be automatically generated.
  * A GPG setup to be able to sign artifacts and the related configuration in ``~/.gitconfig``.
  * A working SSH to the Wikimedia production infrastructure with access to the build and APT hosts.
  * An account on the Wikimedia GitHub organization and a related token with permissions to upload assets to a release.
  * An account on PyPI with release permissions for the package and a related token.
